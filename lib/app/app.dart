import 'package:auth_zicare/features/auth/bloc/auth_bloc.dart';
import 'package:auth_zicare/features/auth/resource/auth_repository.dart';
import 'package:auth_zicare/features/auth/ui/landing.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:auth_zicare/common/constant/env.dart';
import 'package:auth_zicare/common/http/api_provider.dart';
import 'package:google_fonts/google_fonts.dart';

class MyApp extends StatelessWidget {
  MyApp({Key key, @required this.env}) : super(key: key);
  Env env;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<ApiProvider>(
          create: (BuildContext context) => ApiProvider(env: env),
        ),
      ],
      child: BlocProvider<AuthBloc>(
        create: (context) => AuthBloc(
          authRepository: AuthRepository(
            apiProvider: RepositoryProvider.of<ApiProvider>(context)
          )
        )..add(
          AuthEvent.checkAuth()
        ),
        child: MaterialApp(
          home: LandingAuth(),
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Color(0xFF1BABE2), // Zi.Care Primary Color (Based On Zi.Care Website)
            textTheme: GoogleFonts.reemKufiTextTheme(
              Theme.of(context).textTheme
            )
          ),
          builder: EasyLoading.init(),
        ),
      )
    );
  }
}
