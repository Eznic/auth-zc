// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'profile_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ProfileEventTearOff {
  const _$ProfileEventTearOff();

// ignore: unused_element
  _ProfileEventGetProfile getProfile() {
    return const _ProfileEventGetProfile();
  }
}

/// @nodoc
// ignore: unused_element
const $ProfileEvent = _$ProfileEventTearOff();

/// @nodoc
mixin _$ProfileEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getProfile(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getProfile(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getProfile(_ProfileEventGetProfile value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getProfile(_ProfileEventGetProfile value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ProfileEventCopyWith<$Res> {
  factory $ProfileEventCopyWith(
          ProfileEvent value, $Res Function(ProfileEvent) then) =
      _$ProfileEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProfileEventCopyWithImpl<$Res> implements $ProfileEventCopyWith<$Res> {
  _$ProfileEventCopyWithImpl(this._value, this._then);

  final ProfileEvent _value;
  // ignore: unused_field
  final $Res Function(ProfileEvent) _then;
}

/// @nodoc
abstract class _$ProfileEventGetProfileCopyWith<$Res> {
  factory _$ProfileEventGetProfileCopyWith(_ProfileEventGetProfile value,
          $Res Function(_ProfileEventGetProfile) then) =
      __$ProfileEventGetProfileCopyWithImpl<$Res>;
}

/// @nodoc
class __$ProfileEventGetProfileCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res>
    implements _$ProfileEventGetProfileCopyWith<$Res> {
  __$ProfileEventGetProfileCopyWithImpl(_ProfileEventGetProfile _value,
      $Res Function(_ProfileEventGetProfile) _then)
      : super(_value, (v) => _then(v as _ProfileEventGetProfile));

  @override
  _ProfileEventGetProfile get _value => super._value as _ProfileEventGetProfile;
}

/// @nodoc
class _$_ProfileEventGetProfile implements _ProfileEventGetProfile {
  const _$_ProfileEventGetProfile();

  @override
  String toString() {
    return 'ProfileEvent.getProfile()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _ProfileEventGetProfile);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getProfile(),
  }) {
    assert(getProfile != null);
    return getProfile();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getProfile(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getProfile != null) {
      return getProfile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getProfile(_ProfileEventGetProfile value),
  }) {
    assert(getProfile != null);
    return getProfile(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getProfile(_ProfileEventGetProfile value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getProfile != null) {
      return getProfile(this);
    }
    return orElse();
  }
}

abstract class _ProfileEventGetProfile implements ProfileEvent {
  const factory _ProfileEventGetProfile() = _$_ProfileEventGetProfile;
}

/// @nodoc
class _$ProfileStateTearOff {
  const _$ProfileStateTearOff();

// ignore: unused_element
  _ProfileStateInitial initial() {
    return const _ProfileStateInitial();
  }

// ignore: unused_element
  _ProfileStateLoading loading() {
    return const _ProfileStateLoading();
  }

// ignore: unused_element
  _ProfileStateHasData hasData({@required Map<dynamic, dynamic> data}) {
    return _ProfileStateHasData(
      data: data,
    );
  }

// ignore: unused_element
  _ProfileStateFailed failed({@required String message}) {
    return _ProfileStateFailed(
      message: message,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ProfileState = _$ProfileStateTearOff();

/// @nodoc
mixin _$ProfileState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult hasData(Map<dynamic, dynamic> data),
    @required TResult failed(String message),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult hasData(Map<dynamic, dynamic> data),
    TResult failed(String message),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_ProfileStateInitial value),
    @required TResult loading(_ProfileStateLoading value),
    @required TResult hasData(_ProfileStateHasData value),
    @required TResult failed(_ProfileStateFailed value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_ProfileStateInitial value),
    TResult loading(_ProfileStateLoading value),
    TResult hasData(_ProfileStateHasData value),
    TResult failed(_ProfileStateFailed value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ProfileStateCopyWith<$Res> {
  factory $ProfileStateCopyWith(
          ProfileState value, $Res Function(ProfileState) then) =
      _$ProfileStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProfileStateCopyWithImpl<$Res> implements $ProfileStateCopyWith<$Res> {
  _$ProfileStateCopyWithImpl(this._value, this._then);

  final ProfileState _value;
  // ignore: unused_field
  final $Res Function(ProfileState) _then;
}

/// @nodoc
abstract class _$ProfileStateInitialCopyWith<$Res> {
  factory _$ProfileStateInitialCopyWith(_ProfileStateInitial value,
          $Res Function(_ProfileStateInitial) then) =
      __$ProfileStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$ProfileStateInitialCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$ProfileStateInitialCopyWith<$Res> {
  __$ProfileStateInitialCopyWithImpl(
      _ProfileStateInitial _value, $Res Function(_ProfileStateInitial) _then)
      : super(_value, (v) => _then(v as _ProfileStateInitial));

  @override
  _ProfileStateInitial get _value => super._value as _ProfileStateInitial;
}

/// @nodoc
class _$_ProfileStateInitial implements _ProfileStateInitial {
  const _$_ProfileStateInitial();

  @override
  String toString() {
    return 'ProfileState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _ProfileStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult hasData(Map<dynamic, dynamic> data),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult hasData(Map<dynamic, dynamic> data),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_ProfileStateInitial value),
    @required TResult loading(_ProfileStateLoading value),
    @required TResult hasData(_ProfileStateHasData value),
    @required TResult failed(_ProfileStateFailed value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_ProfileStateInitial value),
    TResult loading(_ProfileStateLoading value),
    TResult hasData(_ProfileStateHasData value),
    TResult failed(_ProfileStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _ProfileStateInitial implements ProfileState {
  const factory _ProfileStateInitial() = _$_ProfileStateInitial;
}

/// @nodoc
abstract class _$ProfileStateLoadingCopyWith<$Res> {
  factory _$ProfileStateLoadingCopyWith(_ProfileStateLoading value,
          $Res Function(_ProfileStateLoading) then) =
      __$ProfileStateLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$ProfileStateLoadingCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$ProfileStateLoadingCopyWith<$Res> {
  __$ProfileStateLoadingCopyWithImpl(
      _ProfileStateLoading _value, $Res Function(_ProfileStateLoading) _then)
      : super(_value, (v) => _then(v as _ProfileStateLoading));

  @override
  _ProfileStateLoading get _value => super._value as _ProfileStateLoading;
}

/// @nodoc
class _$_ProfileStateLoading implements _ProfileStateLoading {
  const _$_ProfileStateLoading();

  @override
  String toString() {
    return 'ProfileState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _ProfileStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult hasData(Map<dynamic, dynamic> data),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult hasData(Map<dynamic, dynamic> data),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_ProfileStateInitial value),
    @required TResult loading(_ProfileStateLoading value),
    @required TResult hasData(_ProfileStateHasData value),
    @required TResult failed(_ProfileStateFailed value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_ProfileStateInitial value),
    TResult loading(_ProfileStateLoading value),
    TResult hasData(_ProfileStateHasData value),
    TResult failed(_ProfileStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _ProfileStateLoading implements ProfileState {
  const factory _ProfileStateLoading() = _$_ProfileStateLoading;
}

/// @nodoc
abstract class _$ProfileStateHasDataCopyWith<$Res> {
  factory _$ProfileStateHasDataCopyWith(_ProfileStateHasData value,
          $Res Function(_ProfileStateHasData) then) =
      __$ProfileStateHasDataCopyWithImpl<$Res>;
  $Res call({Map<dynamic, dynamic> data});
}

/// @nodoc
class __$ProfileStateHasDataCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$ProfileStateHasDataCopyWith<$Res> {
  __$ProfileStateHasDataCopyWithImpl(
      _ProfileStateHasData _value, $Res Function(_ProfileStateHasData) _then)
      : super(_value, (v) => _then(v as _ProfileStateHasData));

  @override
  _ProfileStateHasData get _value => super._value as _ProfileStateHasData;

  @override
  $Res call({
    Object data = freezed,
  }) {
    return _then(_ProfileStateHasData(
      data: data == freezed ? _value.data : data as Map<dynamic, dynamic>,
    ));
  }
}

/// @nodoc
class _$_ProfileStateHasData implements _ProfileStateHasData {
  const _$_ProfileStateHasData({@required this.data}) : assert(data != null);

  @override
  final Map<dynamic, dynamic> data;

  @override
  String toString() {
    return 'ProfileState.hasData(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ProfileStateHasData &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @JsonKey(ignore: true)
  @override
  _$ProfileStateHasDataCopyWith<_ProfileStateHasData> get copyWith =>
      __$ProfileStateHasDataCopyWithImpl<_ProfileStateHasData>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult hasData(Map<dynamic, dynamic> data),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return hasData(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult hasData(Map<dynamic, dynamic> data),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (hasData != null) {
      return hasData(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_ProfileStateInitial value),
    @required TResult loading(_ProfileStateLoading value),
    @required TResult hasData(_ProfileStateHasData value),
    @required TResult failed(_ProfileStateFailed value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return hasData(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_ProfileStateInitial value),
    TResult loading(_ProfileStateLoading value),
    TResult hasData(_ProfileStateHasData value),
    TResult failed(_ProfileStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (hasData != null) {
      return hasData(this);
    }
    return orElse();
  }
}

abstract class _ProfileStateHasData implements ProfileState {
  const factory _ProfileStateHasData({@required Map<dynamic, dynamic> data}) =
      _$_ProfileStateHasData;

  Map<dynamic, dynamic> get data;
  @JsonKey(ignore: true)
  _$ProfileStateHasDataCopyWith<_ProfileStateHasData> get copyWith;
}

/// @nodoc
abstract class _$ProfileStateFailedCopyWith<$Res> {
  factory _$ProfileStateFailedCopyWith(
          _ProfileStateFailed value, $Res Function(_ProfileStateFailed) then) =
      __$ProfileStateFailedCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$ProfileStateFailedCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res>
    implements _$ProfileStateFailedCopyWith<$Res> {
  __$ProfileStateFailedCopyWithImpl(
      _ProfileStateFailed _value, $Res Function(_ProfileStateFailed) _then)
      : super(_value, (v) => _then(v as _ProfileStateFailed));

  @override
  _ProfileStateFailed get _value => super._value as _ProfileStateFailed;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(_ProfileStateFailed(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$_ProfileStateFailed implements _ProfileStateFailed {
  const _$_ProfileStateFailed({@required this.message})
      : assert(message != null);

  @override
  final String message;

  @override
  String toString() {
    return 'ProfileState.failed(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ProfileStateFailed &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$ProfileStateFailedCopyWith<_ProfileStateFailed> get copyWith =>
      __$ProfileStateFailedCopyWithImpl<_ProfileStateFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult hasData(Map<dynamic, dynamic> data),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return failed(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult hasData(Map<dynamic, dynamic> data),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failed != null) {
      return failed(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_ProfileStateInitial value),
    @required TResult loading(_ProfileStateLoading value),
    @required TResult hasData(_ProfileStateHasData value),
    @required TResult failed(_ProfileStateFailed value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(hasData != null);
    assert(failed != null);
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_ProfileStateInitial value),
    TResult loading(_ProfileStateLoading value),
    TResult hasData(_ProfileStateHasData value),
    TResult failed(_ProfileStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class _ProfileStateFailed implements ProfileState {
  const factory _ProfileStateFailed({@required String message}) =
      _$_ProfileStateFailed;

  String get message;
  @JsonKey(ignore: true)
  _$ProfileStateFailedCopyWith<_ProfileStateFailed> get copyWith;
}
