import 'dart:async';

import 'package:auth_zicare/features/profile/resource/profile_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_event.dart';
part 'profile_state.dart';
part 'profile_bloc.freezed.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc({
    @required this.profileRepository
  }) : super(_ProfileStateInitial());

  ProfileRepository profileRepository;

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    yield* event.map(
      getProfile: (value) async* {
        yield ProfileState.loading();

        try {

          final Response response = await profileRepository.getProfile();

          if( response.statusCode == 200 ) {
            yield ProfileState.hasData(data: response.data);
          } else {
            print('Response Failed : ${response.data}');
            yield ProfileState.failed(message: response.data['message']);
          }

        } catch(e) {
          yield ProfileState.failed(message: 'Error : $e');
        }
      }
    );
  }
}
