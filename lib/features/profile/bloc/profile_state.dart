part of 'profile_bloc.dart';

@freezed
abstract class ProfileState with _$ProfileState {
  const factory ProfileState.initial() = _ProfileStateInitial;
  const factory ProfileState.loading() = _ProfileStateLoading;
  const factory ProfileState.hasData({
    @required Map data
  }) = _ProfileStateHasData;
  const factory ProfileState.failed({
    @required String message
  }) = _ProfileStateFailed;
}
