import 'package:auth_zicare/common/http/api_provider.dart';
import 'package:auth_zicare/features/auth/bloc/auth_bloc.dart';
import 'package:auth_zicare/features/auth/ui/auth_ui.dart';
import 'package:auth_zicare/features/profile/bloc/profile_bloc.dart';
import 'package:auth_zicare/features/profile/resource/profile_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:transparent_image/transparent_image.dart';

class ProfileUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return RepositoryProvider(
      create: (context) => ProfileRepository(
        apiProvider: RepositoryProvider.of<ApiProvider>(context)
      ),
      child: BlocProvider(
        create: (context) => ProfileBloc(
          profileRepository: RepositoryProvider.of<ProfileRepository>(context)
        )..add(
          ProfileEvent.getProfile()
        ),
        child: Scaffold(
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                Positioned(
                  bottom: -300,
                  right: 0,
                  child: Container(
                    width: 300,
                    child: SvgPicture.asset('assets/svg/bgr.svg'),
                  ),
                ),
                BlocBuilder<ProfileBloc, ProfileState>(
                  builder: (context, state) {
                    return state.maybeMap(
                      loading: (value) {
                        EasyLoading.show(status: 'Loading...');
                        return Container();
                      },
                      hasData: (value) {
                        EasyLoading.dismiss();
                        print(value.data['data']);
                        return profileMainBodyWidget(context, value.data['data']);
                      },
                      orElse: () {
                        EasyLoading.dismiss();
                        return Center(
                          child: Text('Something Went Wrong'),
                        );
                      } 
                    );
                  },
                ),
              ],
            )
          ),
        ),
      ),
    );
  }

  Widget profileMainBodyWidget(BuildContext context, Map data) {

    Size size = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        SizedBox(
          height: size.height / 10,
        ),

        Center(
          child: Text(
            'Profile',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 40,
              fontWeight: FontWeight.bold
            ),
          ),
        ),

        SizedBox(height: 20),

        Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(10, 40, 10, 20),
              width: size.width / 1.2,
              height: size.height / 2.35,
              child: Container(
                width: size.width / 1.5,
                height: size.width / 1,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.blueGrey,
                    width: 2
                  ),
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(20, 25, 20, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 30),
                      Text(
                        'Name',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(
                        data['user']['full_name'],
                        style: TextStyle(
                          fontSize: 17
                        ),
                      ),

                      SizedBox(height: 15),
                      
                      Text(
                        'Username',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(
                        data['user']['username'],
                        style: TextStyle(
                          fontSize: 17
                        ),
                      ),

                      SizedBox(height: 15),

                      Text(
                        'Email',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(
                        data['user']['email'],
                        style: TextStyle(
                          fontSize: 17
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),

            Positioned.fill(
              child: Align(
                alignment: Alignment.topCenter,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Container(
                    width: size.width / 4,
                    height: size.width / 4,
                    color: Colors.white,
                    child: Builder(
                      builder: (context) {

                        if( data['user']['avatar'] == null ) {
                          return Icon(
                            Icons.account_circle,
                            size: size.width / 4,
                          );
                        }

                        return FadeInImage.memoryNetwork(
                          placeholder: kTransparentImage, 
                          image: data['user']['avatar'], // I Hope Avatar Is An Image
                          fit: BoxFit.cover,
                        );

                      },
                    ),
                  ),
                ),
              )
            ),
          ],
        ),

        SizedBox(height: 15),

        FlatButton(
          color: Colors.red,
          onPressed: () {
            BlocProvider.of<AuthBloc>(context).add(
              AuthEvent.logout()
            );

            Navigator.of(context).pushReplacement(
              CupertinoPageRoute(
                builder: (context) => AuthUI()
              )
            );
          },
          child: Text(
            'Logout',
            style: TextStyle(
              color: Colors.white,
              fontSize: 17
            ),
          ),
        )
      ],
    );

  }
}