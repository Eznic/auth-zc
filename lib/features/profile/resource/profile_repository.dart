import 'dart:async';
import 'package:auth_zicare/common/http/api_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';

import 'profile_api_provider.dart';

class ProfileRepository {

  ProfileRepository({
    @required this.apiProvider,
  }) { 
    assert(apiProvider != null);
    flutterSecureStorage = FlutterSecureStorage();
  }

  ApiProvider apiProvider;
  ProfileApiProvider profileApiProvider;
  FlutterSecureStorage flutterSecureStorage;

  Future getProfile() async {
    await initializeApiProvider();
    return await profileApiProvider.getProfile();
  }

  Future initializeApiProvider() async {
    String token = await flutterSecureStorage.read(key: 'token');
    apiProvider.setToken(token);
    
    profileApiProvider = ProfileApiProvider(apiProvider: apiProvider);
  }

}