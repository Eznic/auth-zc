import 'dart:async';
import 'package:auth_zicare/common/http/api_provider.dart';
import 'package:meta/meta.dart';

class ProfileApiProvider {

  ProfileApiProvider({
    @required this.apiProvider
  }) : assert(apiProvider != null);

  ApiProvider apiProvider;

  Future getProfile() async {

    try {

      return await apiProvider.get(
        endpoint: 'api/v1/users/me/',
      );

    }
    catch(e) {
      throw Exception('Failed To load GET ' + e.toString());
    }

  }

}