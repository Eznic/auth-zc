import 'dart:async';
import 'package:auth_zicare/common/http/api_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';

import 'auth_api_provider.dart';

class AuthRepository {

  AuthRepository({
    @required this.apiProvider,
  }) { 
    assert(apiProvider != null);
    initializeApiProvider();
    flutterSecureStorage = FlutterSecureStorage();
  }

  ApiProvider apiProvider;
  AuthApiProvider authApiProvider;
  FlutterSecureStorage flutterSecureStorage;

  Future login({ String username, String password, String gType }) async {
    
    Map<String, String> data = {
      'username': username,
      'password': password,
      'grant_type': gType
    };

    Response response = await authApiProvider.login(data);

    if( response.statusCode == 200 ) {
      Map data = response.data;

      // Token
      await flutterSecureStorage.write(
        key: 'token', 
        value: data['access_token']
      );
      // Token Expired Date
      await flutterSecureStorage.write(
        key: 'token_exp', 
        value: DateTime.now().add(
          Duration(
            seconds: data['expires_in']
          )
        ).millisecondsSinceEpoch.toString()
      );

    }

    return response;
  }

  Future register({ String fName, String username, String email, String password }) async {
    
    Map<String, String> data = {
      'full_name': fName,
      'username': username,
      'email': email,
      'password': password,
    };

    return await authApiProvider.register(data);
  }

  Future checkAuth() async {
    String token = await flutterSecureStorage.read(key: 'token');

    if( token != null ) {
      String expires = await flutterSecureStorage.read(key: 'token_exp');
      String date = DateTime.now().millisecondsSinceEpoch.toString();

      if( int.parse(date) < int.parse(expires) ) {
        return true;
      }
    }

    return false;
  }

  Future logout() async {
    await flutterSecureStorage.deleteAll();
  }

  void initializeApiProvider() {
    authApiProvider = AuthApiProvider(apiProvider: apiProvider);
  }

}