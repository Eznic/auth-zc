import 'dart:async';
import 'package:auth_zicare/common/http/api_provider.dart';
import 'package:meta/meta.dart';

class AuthApiProvider {

  AuthApiProvider({
    @required this.apiProvider
  }) : assert(apiProvider != null);

  ApiProvider apiProvider;

  Future register(Map data) async {

    try {

      return await apiProvider.post(
        endpoint: 'api/v1/auth/register',
        data: data
      );

    }
    catch(e) {
      throw Exception('Failed To load POST ' + e.toString());
    }

  }

  Future login(Map data) async {

    try {

      return await apiProvider.post(
        endpoint: 'api/v1/auth/token',
        data: data
      );

    }
    catch(e) {
      throw Exception('Failed To load POST ' + e.toString());
    }

  }

}