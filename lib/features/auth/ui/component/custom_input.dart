import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {

  CustomInput({this.hint, this.controller, this.isPassword = false});

  String hint;
  TextEditingController controller;
  bool isPassword;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width / 1.2,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).primaryColor
        ),
        borderRadius: BorderRadius.circular(20)
      ),
      child: TextField(
        controller: controller,
        obscureText: isPassword,
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(
            color: Theme.of(context).primaryColor
          ),
          enabledBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
        ),
      ),
    );
  }

}

Widget customInput(context, {String hint, TextEditingController controller, bool isPassword = false}) {
    
  }