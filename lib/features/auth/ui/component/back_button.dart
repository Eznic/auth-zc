import 'package:flutter/material.dart';

class CBackButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(
            Icons.keyboard_arrow_left,
            color: Theme.of(context).primaryColor,
            size: 40,
          ),
          Text(
            'Back',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 18
            )
          ),
        ]
      )
    );
  }

}