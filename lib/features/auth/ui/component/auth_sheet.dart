import 'package:auth_zicare/features/auth/ui/page/login_ui.dart';
import 'package:auth_zicare/features/auth/ui/page/register_ui.dart';
import 'package:flutter/material.dart';

class AuthSheet extends StatelessWidget {

  AuthSheet({
    @required this.type
  });

  AuthSheetType type;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(40), 
        topRight: Radius.circular(40)
      ),
      child: Container(
        color: Colors.white,
        child: Builder(
          builder: (context) {
            Widget page = type == AuthSheetType.login ? LoginUI() : RegisterUI();

            return SingleChildScrollView(
              child: page
            );
          },
        )
      ),
    );
  }

}

enum AuthSheetType { login, register }