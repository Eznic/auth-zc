import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {

  CustomButton({
    this.onTap,
    this.text,
    this.color,
    this.textColor,
    this.type
  });

  Function onTap;
  String text;
  ButtonType type;
  Color textColor;
  Color color;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        color: type == ButtonType.outline ? Colors.transparent : color,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          customBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
          ),
          onTap: onTap ?? () {},
          child: Container(
            width: size.width,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(
                color: type == ButtonType.outline ? textColor : color,
                width: 2
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Center(
              child: Text(
                text,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: textColor
                ),
              ),
            ),
          )
        ),
      ),
    );
  }
}

enum ButtonType { outline, filled }