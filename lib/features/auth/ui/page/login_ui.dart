import 'package:auth_zicare/features/auth/bloc/auth_bloc.dart';
import 'package:auth_zicare/features/auth/ui/component/back_button.dart';
import 'package:auth_zicare/features/auth/ui/component/button.dart';
import 'package:auth_zicare/features/auth/ui/component/custom_input.dart';
import 'package:auth_zicare/features/profile/ui/profile_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginUI extends StatefulWidget {
  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUI> {
  TextEditingController uController = TextEditingController();
  TextEditingController pController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Builder(
      builder: (context) {
        return BlocListener<AuthBloc, AuthState>(
          listener: (context, state) {
            state.maybeMap(
              authorized: (value) {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (context) => ProfileUI() 
                  )
                );
              },
              orElse: () {}
            );
          },
          child: Container(
            height: size.height / 1.2,
            width: size.width,
            padding: EdgeInsets.all(10),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CBackButton(),
                  SizedBox(height: size.height / 20),
                  Center(
                    child: Column(
                      children: <Widget>[
                        // Title
                        Text(
                          'Login',
                          style: TextStyle(
                              color: Theme.of(context).primaryColor.withRed(3),
                              fontSize: 40,
                              fontWeight: FontWeight.bold),
                        ),

                        SizedBox(height: 20),

                        // Username
                        CustomInput(hint: 'Username', controller: uController),
                        SizedBox(height: 10),

                        // Password
                        CustomInput(
                            isPassword: true,
                            hint: 'Password',
                            controller: pController),
                        SizedBox(height: 15),

                        Container(
                          width: size.width / 1.7,
                          child: CustomButton(
                            onTap: () {
                              BlocProvider.of<AuthBloc>(context).add(
                                  AuthEvent.login(
                                      username: uController.text,
                                      password: pController.text,
                                      gType: 'password'));
                            },
                            color: Theme.of(context).primaryColor,
                            text: 'Submit',
                            textColor: Colors.white,
                            type: ButtonType.filled,
                          ),
                        ),

                        SizedBox(height: 40),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Don't Have Any Account Yet ?",
                              style: TextStyle(color: Colors.blueGrey),
                            ),
                            TextButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text(
                                  'Register Here',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ))
                          ],
                        ),

                        BlocListener<AuthBloc, AuthState>(
                          listener: (context, state) {
                            state.maybeMap(loading: (value) {
                              EasyLoading.show(status: 'Loading...');
                            },
                            success: (value) {
                              EasyLoading.dismiss();
                              Navigator.of(context).pushAndRemoveUntil(
                                CupertinoPageRoute(
                                  builder: (context) => ProfileUI()
                                ), 
                                (route) => false
                              );
                            }, 
                            failed: (value) {
                              EasyLoading.dismiss();
                              Fluttertoast.showToast(msg: value.message);
                            }, 
                            orElse: () {
                              EasyLoading.dismiss();
                            });
                          },
                          child: Container(),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
