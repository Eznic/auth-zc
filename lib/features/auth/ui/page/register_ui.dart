import 'package:auth_zicare/features/auth/bloc/auth_bloc.dart';
import 'package:auth_zicare/features/auth/ui/component/back_button.dart';
import 'package:auth_zicare/features/auth/ui/component/button.dart';
import 'package:auth_zicare/features/auth/ui/component/custom_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterUI extends StatelessWidget {
  TextEditingController nController = TextEditingController();
  TextEditingController uController = TextEditingController();
  TextEditingController eController = TextEditingController();
  TextEditingController pController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      height: size.height / 1.2,
      width: size.width,
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CBackButton(),
            SizedBox(height: size.height / 20),
            Center(
              child: Column(
                children: <Widget>[
                  // Title
                  Text(
                    'Register',
                    style: TextStyle(
                      color: Theme.of(context).primaryColor.withRed(3),
                      fontSize: 40,
                      fontWeight: FontWeight.bold
                    ),
                  ),

                  SizedBox(height: 20),

                  // Fullname
                  CustomInput(controller: nController, hint: 'Full Name'),
                  SizedBox(height: 10),

                  // Username
                  CustomInput(controller: uController, hint: 'Username'),
                  SizedBox(height: 10),

                  // Email
                  CustomInput(controller: eController, hint: 'Email'),
                  SizedBox(height: 10),

                  // Password
                  CustomInput(controller: pController, isPassword: true, hint: 'Password'),
                  SizedBox(height: 15),

                  Container(
                    width: size.width / 1.7,
                    child: CustomButton(
                      onTap: () {
                        BlocProvider.of<AuthBloc>(context).add(
                          AuthEvent.register(
                            fName: nController.text,
                            username: uController.text,
                            email: eController.text,
                            password: pController.text,
                          )
                        );
                      },
                      color: Theme.of(context).primaryColor,
                      text: 'Submit',
                      textColor: Colors.white,
                      type: ButtonType.filled,
                    ),
                  ),

                  BlocListener<AuthBloc, AuthState>(
                    listener: (context, state) {
                      state.maybeMap(
                        loading: (value) {
                          EasyLoading.show(status: 'Loading...');
                        },
                        success: (value) {
                          EasyLoading.dismiss();
                          Navigator.of(context).pop();
                        },
                        failed: (value) {
                          EasyLoading.dismiss();
                          Fluttertoast.showToast(msg: value.message);
                        },
                        orElse: () {
                          EasyLoading.dismiss();
                        }
                      );
                    },
                    child: Container(),
                  )

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}