import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'component/auth_sheet.dart';
import 'component/button.dart';

class AuthUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Builder(
        builder: (context) {
          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: size.height / 8.5),
                
                // Zi.Care Logo
                Container(
                  width: 200,
                  child: SvgPicture.asset(
                    'assets/svg/logo.svg',
                    color: Colors.white,
                  ),
                ),

                SizedBox(height: 10),

                // Login Or Register Button
                Center(
                  child: Container(
                    width: 300,
                    child: Column(
                      children: <Widget>[
                        CustomButton(
                          text: 'Login',
                          color: Colors.white,
                          textColor: Theme.of(context).primaryColor,
                          type: ButtonType.filled,
                          onTap: () => showLoginSheet(context),
                        ),
                        SizedBox(height:  10),
                        CustomButton(
                          text: 'Register',
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                          type: ButtonType.outline,
                          onTap: () => showRegisterSheet(context),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }
      )
    );
  }

  void showLoginSheet(context) {
    showBottomSheet<void>(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return AuthSheet(type: AuthSheetType.login);
      },
    );
  }

  void showRegisterSheet(context) {
    showBottomSheet<void>(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return AuthSheet(type: AuthSheetType.register);
      },
    );
  }
}