import 'package:auth_zicare/features/auth/bloc/auth_bloc.dart';
import 'package:auth_zicare/features/auth/ui/auth_ui.dart';
import 'package:auth_zicare/features/profile/ui/profile_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class LandingAuth extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        print('Current Auth State $state');
        state.maybeMap(
          authorized: (value) {
            EasyLoading.dismiss();
            Navigator.of(context).pushReplacement(
              CupertinoPageRoute(
                builder: (context) => ProfileUI()
              )
            );
          },
          unAuthorized: (value) {
            EasyLoading.dismiss();
            Navigator.of(context).pushReplacement(
              CupertinoPageRoute(
                builder: (context) => AuthUI()
              )
            );
          },
          orElse: () {
            EasyLoading.show(
              status: 'Loading...'
            );
          }
        );
      },
      child: Container(),
    );

  }
}