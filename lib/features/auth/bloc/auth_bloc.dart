import 'dart:async';

import 'package:auth_zicare/features/auth/resource/auth_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_event.dart';
part 'auth_state.dart';
part 'auth_bloc.freezed.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc({
    @required this.authRepository
  }) : super(_AuthStateInitial());

  AuthRepository authRepository;

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    yield* event.map(
      checkAuth: (value) async* {
        yield AuthState.loading();

        try {
          
          final bool result = await authRepository.checkAuth();

          if( result ) {
            yield AuthState.authorized();
          } else {
            yield AuthState.unAuthorized();
          }

        } catch(e) {
          yield AuthState.failed(message: 'Error : $e');
        }
      },
      login: (value) async* {
        yield AuthState.loading();

        try {
          
          final Response response = await authRepository.login(
            username: value.username,
            password: value.password,
            gType: value.gType
          );

          if( response.statusCode == 200 ) {
            yield AuthState.success();
          } else {
            yield AuthState.failed(message: response.data['message']);
          }

        } catch(e) {
          yield AuthState.failed(message: 'Error : $e');
        }
      }, 
      register: (value) async* {
        yield AuthState.loading();

        try {
          
          final Response response = await authRepository.register(
            fName: value.fName,
            username: value.username,
            email: value.email,
            password: value.password,
          );

          if( response.statusCode == 200 ) {
            yield AuthState.success();
          } else {
            yield AuthState.failed(message: response.data['message']);
          }

        } catch(e) {
          yield AuthState.failed(message: 'Error : $e');
        }
      },
      logout: (value) async* {
        yield AuthState.loading();

        try {
          
          await authRepository.logout();

          yield AuthState.success();

        } catch(e) {
          yield AuthState.failed(message: 'Error : $e');
        }
      }
    );
  }
}
