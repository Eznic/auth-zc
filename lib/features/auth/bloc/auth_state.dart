part of 'auth_bloc.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState.initial() = _AuthStateInitial;
  const factory AuthState.unAuthorized() = _AuthStateUnAuthorized;
  const factory AuthState.authorized() = _AuthStateAuthorized;
  const factory AuthState.loading() = _AuthStateLoading;
  const factory AuthState.success() = _AuthStateSuccess;
  const factory AuthState.failed({
    @required String message
  }) = _AuthStateFailed;
}
