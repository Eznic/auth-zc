// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AuthEventTearOff {
  const _$AuthEventTearOff();

// ignore: unused_element
  _AuthEventLogin login({String username, String password, String gType}) {
    return _AuthEventLogin(
      username: username,
      password: password,
      gType: gType,
    );
  }

// ignore: unused_element
  _AuthEventRegister register(
      {String fName, String username, String email, String password}) {
    return _AuthEventRegister(
      fName: fName,
      username: username,
      email: email,
      password: password,
    );
  }

// ignore: unused_element
  _AuthEventCheck checkAuth() {
    return const _AuthEventCheck();
  }

// ignore: unused_element
  _AuthEventLogout logout() {
    return const _AuthEventLogout();
  }
}

/// @nodoc
// ignore: unused_element
const $AuthEvent = _$AuthEventTearOff();

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult login(String username, String password, String gType),
    @required
        TResult register(
            String fName, String username, String email, String password),
    @required TResult checkAuth(),
    @required TResult logout(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult login(String username, String password, String gType),
    TResult register(
        String fName, String username, String email, String password),
    TResult checkAuth(),
    TResult logout(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult login(_AuthEventLogin value),
    @required TResult register(_AuthEventRegister value),
    @required TResult checkAuth(_AuthEventCheck value),
    @required TResult logout(_AuthEventLogout value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult login(_AuthEventLogin value),
    TResult register(_AuthEventRegister value),
    TResult checkAuth(_AuthEventCheck value),
    TResult logout(_AuthEventLogout value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res> implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  final AuthEvent _value;
  // ignore: unused_field
  final $Res Function(AuthEvent) _then;
}

/// @nodoc
abstract class _$AuthEventLoginCopyWith<$Res> {
  factory _$AuthEventLoginCopyWith(
          _AuthEventLogin value, $Res Function(_AuthEventLogin) then) =
      __$AuthEventLoginCopyWithImpl<$Res>;
  $Res call({String username, String password, String gType});
}

/// @nodoc
class __$AuthEventLoginCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventLoginCopyWith<$Res> {
  __$AuthEventLoginCopyWithImpl(
      _AuthEventLogin _value, $Res Function(_AuthEventLogin) _then)
      : super(_value, (v) => _then(v as _AuthEventLogin));

  @override
  _AuthEventLogin get _value => super._value as _AuthEventLogin;

  @override
  $Res call({
    Object username = freezed,
    Object password = freezed,
    Object gType = freezed,
  }) {
    return _then(_AuthEventLogin(
      username: username == freezed ? _value.username : username as String,
      password: password == freezed ? _value.password : password as String,
      gType: gType == freezed ? _value.gType : gType as String,
    ));
  }
}

/// @nodoc
class _$_AuthEventLogin implements _AuthEventLogin {
  const _$_AuthEventLogin({this.username, this.password, this.gType});

  @override
  final String username;
  @override
  final String password;
  @override
  final String gType;

  @override
  String toString() {
    return 'AuthEvent.login(username: $username, password: $password, gType: $gType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AuthEventLogin &&
            (identical(other.username, username) ||
                const DeepCollectionEquality()
                    .equals(other.username, username)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.gType, gType) ||
                const DeepCollectionEquality().equals(other.gType, gType)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(username) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(gType);

  @JsonKey(ignore: true)
  @override
  _$AuthEventLoginCopyWith<_AuthEventLogin> get copyWith =>
      __$AuthEventLoginCopyWithImpl<_AuthEventLogin>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult login(String username, String password, String gType),
    @required
        TResult register(
            String fName, String username, String email, String password),
    @required TResult checkAuth(),
    @required TResult logout(),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return login(username, password, gType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult login(String username, String password, String gType),
    TResult register(
        String fName, String username, String email, String password),
    TResult checkAuth(),
    TResult logout(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (login != null) {
      return login(username, password, gType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult login(_AuthEventLogin value),
    @required TResult register(_AuthEventRegister value),
    @required TResult checkAuth(_AuthEventCheck value),
    @required TResult logout(_AuthEventLogout value),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return login(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult login(_AuthEventLogin value),
    TResult register(_AuthEventRegister value),
    TResult checkAuth(_AuthEventCheck value),
    TResult logout(_AuthEventLogout value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (login != null) {
      return login(this);
    }
    return orElse();
  }
}

abstract class _AuthEventLogin implements AuthEvent {
  const factory _AuthEventLogin(
      {String username, String password, String gType}) = _$_AuthEventLogin;

  String get username;
  String get password;
  String get gType;
  @JsonKey(ignore: true)
  _$AuthEventLoginCopyWith<_AuthEventLogin> get copyWith;
}

/// @nodoc
abstract class _$AuthEventRegisterCopyWith<$Res> {
  factory _$AuthEventRegisterCopyWith(
          _AuthEventRegister value, $Res Function(_AuthEventRegister) then) =
      __$AuthEventRegisterCopyWithImpl<$Res>;
  $Res call({String fName, String username, String email, String password});
}

/// @nodoc
class __$AuthEventRegisterCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventRegisterCopyWith<$Res> {
  __$AuthEventRegisterCopyWithImpl(
      _AuthEventRegister _value, $Res Function(_AuthEventRegister) _then)
      : super(_value, (v) => _then(v as _AuthEventRegister));

  @override
  _AuthEventRegister get _value => super._value as _AuthEventRegister;

  @override
  $Res call({
    Object fName = freezed,
    Object username = freezed,
    Object email = freezed,
    Object password = freezed,
  }) {
    return _then(_AuthEventRegister(
      fName: fName == freezed ? _value.fName : fName as String,
      username: username == freezed ? _value.username : username as String,
      email: email == freezed ? _value.email : email as String,
      password: password == freezed ? _value.password : password as String,
    ));
  }
}

/// @nodoc
class _$_AuthEventRegister implements _AuthEventRegister {
  const _$_AuthEventRegister(
      {this.fName, this.username, this.email, this.password});

  @override
  final String fName;
  @override
  final String username;
  @override
  final String email;
  @override
  final String password;

  @override
  String toString() {
    return 'AuthEvent.register(fName: $fName, username: $username, email: $email, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AuthEventRegister &&
            (identical(other.fName, fName) ||
                const DeepCollectionEquality().equals(other.fName, fName)) &&
            (identical(other.username, username) ||
                const DeepCollectionEquality()
                    .equals(other.username, username)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fName) ^
      const DeepCollectionEquality().hash(username) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password);

  @JsonKey(ignore: true)
  @override
  _$AuthEventRegisterCopyWith<_AuthEventRegister> get copyWith =>
      __$AuthEventRegisterCopyWithImpl<_AuthEventRegister>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult login(String username, String password, String gType),
    @required
        TResult register(
            String fName, String username, String email, String password),
    @required TResult checkAuth(),
    @required TResult logout(),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return register(fName, username, email, password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult login(String username, String password, String gType),
    TResult register(
        String fName, String username, String email, String password),
    TResult checkAuth(),
    TResult logout(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (register != null) {
      return register(fName, username, email, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult login(_AuthEventLogin value),
    @required TResult register(_AuthEventRegister value),
    @required TResult checkAuth(_AuthEventCheck value),
    @required TResult logout(_AuthEventLogout value),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return register(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult login(_AuthEventLogin value),
    TResult register(_AuthEventRegister value),
    TResult checkAuth(_AuthEventCheck value),
    TResult logout(_AuthEventLogout value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (register != null) {
      return register(this);
    }
    return orElse();
  }
}

abstract class _AuthEventRegister implements AuthEvent {
  const factory _AuthEventRegister(
      {String fName,
      String username,
      String email,
      String password}) = _$_AuthEventRegister;

  String get fName;
  String get username;
  String get email;
  String get password;
  @JsonKey(ignore: true)
  _$AuthEventRegisterCopyWith<_AuthEventRegister> get copyWith;
}

/// @nodoc
abstract class _$AuthEventCheckCopyWith<$Res> {
  factory _$AuthEventCheckCopyWith(
          _AuthEventCheck value, $Res Function(_AuthEventCheck) then) =
      __$AuthEventCheckCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthEventCheckCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventCheckCopyWith<$Res> {
  __$AuthEventCheckCopyWithImpl(
      _AuthEventCheck _value, $Res Function(_AuthEventCheck) _then)
      : super(_value, (v) => _then(v as _AuthEventCheck));

  @override
  _AuthEventCheck get _value => super._value as _AuthEventCheck;
}

/// @nodoc
class _$_AuthEventCheck implements _AuthEventCheck {
  const _$_AuthEventCheck();

  @override
  String toString() {
    return 'AuthEvent.checkAuth()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthEventCheck);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult login(String username, String password, String gType),
    @required
        TResult register(
            String fName, String username, String email, String password),
    @required TResult checkAuth(),
    @required TResult logout(),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return checkAuth();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult login(String username, String password, String gType),
    TResult register(
        String fName, String username, String email, String password),
    TResult checkAuth(),
    TResult logout(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (checkAuth != null) {
      return checkAuth();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult login(_AuthEventLogin value),
    @required TResult register(_AuthEventRegister value),
    @required TResult checkAuth(_AuthEventCheck value),
    @required TResult logout(_AuthEventLogout value),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return checkAuth(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult login(_AuthEventLogin value),
    TResult register(_AuthEventRegister value),
    TResult checkAuth(_AuthEventCheck value),
    TResult logout(_AuthEventLogout value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (checkAuth != null) {
      return checkAuth(this);
    }
    return orElse();
  }
}

abstract class _AuthEventCheck implements AuthEvent {
  const factory _AuthEventCheck() = _$_AuthEventCheck;
}

/// @nodoc
abstract class _$AuthEventLogoutCopyWith<$Res> {
  factory _$AuthEventLogoutCopyWith(
          _AuthEventLogout value, $Res Function(_AuthEventLogout) then) =
      __$AuthEventLogoutCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthEventLogoutCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements _$AuthEventLogoutCopyWith<$Res> {
  __$AuthEventLogoutCopyWithImpl(
      _AuthEventLogout _value, $Res Function(_AuthEventLogout) _then)
      : super(_value, (v) => _then(v as _AuthEventLogout));

  @override
  _AuthEventLogout get _value => super._value as _AuthEventLogout;
}

/// @nodoc
class _$_AuthEventLogout implements _AuthEventLogout {
  const _$_AuthEventLogout();

  @override
  String toString() {
    return 'AuthEvent.logout()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthEventLogout);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult login(String username, String password, String gType),
    @required
        TResult register(
            String fName, String username, String email, String password),
    @required TResult checkAuth(),
    @required TResult logout(),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return logout();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult login(String username, String password, String gType),
    TResult register(
        String fName, String username, String email, String password),
    TResult checkAuth(),
    TResult logout(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (logout != null) {
      return logout();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult login(_AuthEventLogin value),
    @required TResult register(_AuthEventRegister value),
    @required TResult checkAuth(_AuthEventCheck value),
    @required TResult logout(_AuthEventLogout value),
  }) {
    assert(login != null);
    assert(register != null);
    assert(checkAuth != null);
    assert(logout != null);
    return logout(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult login(_AuthEventLogin value),
    TResult register(_AuthEventRegister value),
    TResult checkAuth(_AuthEventCheck value),
    TResult logout(_AuthEventLogout value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (logout != null) {
      return logout(this);
    }
    return orElse();
  }
}

abstract class _AuthEventLogout implements AuthEvent {
  const factory _AuthEventLogout() = _$_AuthEventLogout;
}

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

// ignore: unused_element
  _AuthStateInitial initial() {
    return const _AuthStateInitial();
  }

// ignore: unused_element
  _AuthStateUnAuthorized unAuthorized() {
    return const _AuthStateUnAuthorized();
  }

// ignore: unused_element
  _AuthStateAuthorized authorized() {
    return const _AuthStateAuthorized();
  }

// ignore: unused_element
  _AuthStateLoading loading() {
    return const _AuthStateLoading();
  }

// ignore: unused_element
  _AuthStateSuccess success() {
    return const _AuthStateSuccess();
  }

// ignore: unused_element
  _AuthStateFailed failed({@required String message}) {
    return _AuthStateFailed(
      message: message,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;
}

/// @nodoc
abstract class _$AuthStateInitialCopyWith<$Res> {
  factory _$AuthStateInitialCopyWith(
          _AuthStateInitial value, $Res Function(_AuthStateInitial) then) =
      __$AuthStateInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthStateInitialCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateInitialCopyWith<$Res> {
  __$AuthStateInitialCopyWithImpl(
      _AuthStateInitial _value, $Res Function(_AuthStateInitial) _then)
      : super(_value, (v) => _then(v as _AuthStateInitial));

  @override
  _AuthStateInitial get _value => super._value as _AuthStateInitial;
}

/// @nodoc
class _$_AuthStateInitial implements _AuthStateInitial {
  const _$_AuthStateInitial();

  @override
  String toString() {
    return 'AuthState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthStateInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _AuthStateInitial implements AuthState {
  const factory _AuthStateInitial() = _$_AuthStateInitial;
}

/// @nodoc
abstract class _$AuthStateUnAuthorizedCopyWith<$Res> {
  factory _$AuthStateUnAuthorizedCopyWith(_AuthStateUnAuthorized value,
          $Res Function(_AuthStateUnAuthorized) then) =
      __$AuthStateUnAuthorizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthStateUnAuthorizedCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateUnAuthorizedCopyWith<$Res> {
  __$AuthStateUnAuthorizedCopyWithImpl(_AuthStateUnAuthorized _value,
      $Res Function(_AuthStateUnAuthorized) _then)
      : super(_value, (v) => _then(v as _AuthStateUnAuthorized));

  @override
  _AuthStateUnAuthorized get _value => super._value as _AuthStateUnAuthorized;
}

/// @nodoc
class _$_AuthStateUnAuthorized implements _AuthStateUnAuthorized {
  const _$_AuthStateUnAuthorized();

  @override
  String toString() {
    return 'AuthState.unAuthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthStateUnAuthorized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return unAuthorized();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unAuthorized != null) {
      return unAuthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return unAuthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unAuthorized != null) {
      return unAuthorized(this);
    }
    return orElse();
  }
}

abstract class _AuthStateUnAuthorized implements AuthState {
  const factory _AuthStateUnAuthorized() = _$_AuthStateUnAuthorized;
}

/// @nodoc
abstract class _$AuthStateAuthorizedCopyWith<$Res> {
  factory _$AuthStateAuthorizedCopyWith(_AuthStateAuthorized value,
          $Res Function(_AuthStateAuthorized) then) =
      __$AuthStateAuthorizedCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthStateAuthorizedCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateAuthorizedCopyWith<$Res> {
  __$AuthStateAuthorizedCopyWithImpl(
      _AuthStateAuthorized _value, $Res Function(_AuthStateAuthorized) _then)
      : super(_value, (v) => _then(v as _AuthStateAuthorized));

  @override
  _AuthStateAuthorized get _value => super._value as _AuthStateAuthorized;
}

/// @nodoc
class _$_AuthStateAuthorized implements _AuthStateAuthorized {
  const _$_AuthStateAuthorized();

  @override
  String toString() {
    return 'AuthState.authorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthStateAuthorized);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return authorized();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authorized != null) {
      return authorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return authorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authorized != null) {
      return authorized(this);
    }
    return orElse();
  }
}

abstract class _AuthStateAuthorized implements AuthState {
  const factory _AuthStateAuthorized() = _$_AuthStateAuthorized;
}

/// @nodoc
abstract class _$AuthStateLoadingCopyWith<$Res> {
  factory _$AuthStateLoadingCopyWith(
          _AuthStateLoading value, $Res Function(_AuthStateLoading) then) =
      __$AuthStateLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthStateLoadingCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateLoadingCopyWith<$Res> {
  __$AuthStateLoadingCopyWithImpl(
      _AuthStateLoading _value, $Res Function(_AuthStateLoading) _then)
      : super(_value, (v) => _then(v as _AuthStateLoading));

  @override
  _AuthStateLoading get _value => super._value as _AuthStateLoading;
}

/// @nodoc
class _$_AuthStateLoading implements _AuthStateLoading {
  const _$_AuthStateLoading();

  @override
  String toString() {
    return 'AuthState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthStateLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _AuthStateLoading implements AuthState {
  const factory _AuthStateLoading() = _$_AuthStateLoading;
}

/// @nodoc
abstract class _$AuthStateSuccessCopyWith<$Res> {
  factory _$AuthStateSuccessCopyWith(
          _AuthStateSuccess value, $Res Function(_AuthStateSuccess) then) =
      __$AuthStateSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$AuthStateSuccessCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateSuccessCopyWith<$Res> {
  __$AuthStateSuccessCopyWithImpl(
      _AuthStateSuccess _value, $Res Function(_AuthStateSuccess) _then)
      : super(_value, (v) => _then(v as _AuthStateSuccess));

  @override
  _AuthStateSuccess get _value => super._value as _AuthStateSuccess;
}

/// @nodoc
class _$_AuthStateSuccess implements _AuthStateSuccess {
  const _$_AuthStateSuccess();

  @override
  String toString() {
    return 'AuthState.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AuthStateSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return success();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _AuthStateSuccess implements AuthState {
  const factory _AuthStateSuccess() = _$_AuthStateSuccess;
}

/// @nodoc
abstract class _$AuthStateFailedCopyWith<$Res> {
  factory _$AuthStateFailedCopyWith(
          _AuthStateFailed value, $Res Function(_AuthStateFailed) then) =
      __$AuthStateFailedCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$AuthStateFailedCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateFailedCopyWith<$Res> {
  __$AuthStateFailedCopyWithImpl(
      _AuthStateFailed _value, $Res Function(_AuthStateFailed) _then)
      : super(_value, (v) => _then(v as _AuthStateFailed));

  @override
  _AuthStateFailed get _value => super._value as _AuthStateFailed;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(_AuthStateFailed(
      message: message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$_AuthStateFailed implements _AuthStateFailed {
  const _$_AuthStateFailed({@required this.message}) : assert(message != null);

  @override
  final String message;

  @override
  String toString() {
    return 'AuthState.failed(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AuthStateFailed &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$AuthStateFailedCopyWith<_AuthStateFailed> get copyWith =>
      __$AuthStateFailedCopyWithImpl<_AuthStateFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult unAuthorized(),
    @required TResult authorized(),
    @required TResult loading(),
    @required TResult success(),
    @required TResult failed(String message),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return failed(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult unAuthorized(),
    TResult authorized(),
    TResult loading(),
    TResult success(),
    TResult failed(String message),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failed != null) {
      return failed(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_AuthStateInitial value),
    @required TResult unAuthorized(_AuthStateUnAuthorized value),
    @required TResult authorized(_AuthStateAuthorized value),
    @required TResult loading(_AuthStateLoading value),
    @required TResult success(_AuthStateSuccess value),
    @required TResult failed(_AuthStateFailed value),
  }) {
    assert(initial != null);
    assert(unAuthorized != null);
    assert(authorized != null);
    assert(loading != null);
    assert(success != null);
    assert(failed != null);
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_AuthStateInitial value),
    TResult unAuthorized(_AuthStateUnAuthorized value),
    TResult authorized(_AuthStateAuthorized value),
    TResult loading(_AuthStateLoading value),
    TResult success(_AuthStateSuccess value),
    TResult failed(_AuthStateFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class _AuthStateFailed implements AuthState {
  const factory _AuthStateFailed({@required String message}) =
      _$_AuthStateFailed;

  String get message;
  @JsonKey(ignore: true)
  _$AuthStateFailedCopyWith<_AuthStateFailed> get copyWith;
}
