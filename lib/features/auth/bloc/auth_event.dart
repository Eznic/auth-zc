part of 'auth_bloc.dart';

@freezed
abstract class AuthEvent with _$AuthEvent {
  const factory AuthEvent.login({
    String username, 
    String password, 
    String gType
  }) = _AuthEventLogin;
  const factory AuthEvent.register({
    String fName,
    String username,
    String email,
    String password,
  }) = _AuthEventRegister;
  const factory AuthEvent.checkAuth() = _AuthEventCheck;
  const factory AuthEvent.logout() = _AuthEventLogout;
}