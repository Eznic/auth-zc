import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:auth_zicare/app/app.dart';
import 'package:auth_zicare/common/constant/env.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FlutterDownloader.initialize();

  runApp(MyApp(
    env: EnvVal.production,
  ));
}