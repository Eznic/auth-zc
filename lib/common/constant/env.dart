class Env {

  Env(this.baseUrl);

  String baseUrl;

}

mixin EnvVal {
  static Env production = Env('https://zpi-dev.zicare.id/');
}