import 'dart:io';
import 'package:html/parser.dart' show parse;
import 'package:dio/dio.dart';
import 'package:auth_zicare/common/constant/env.dart';
import 'package:flutter/material.dart';

class ApiProvider {

  ApiProvider({this.env, this.token = null}) {
    this.dio = new Dio(BaseOptions(
        baseUrl: env.baseUrl,
        connectTimeout: 5000,
        receiveTimeout: 10000,
        followRedirects: false,
        validateStatus: (status) { return status < 500; }
      )
    );
  }

  Dio dio;
  Env env;

  String token;

  Dio get getDio => dio;

  Future get({BuildContext context, String endpoint, Map query}) async {
    try {

      if( token != null ) dio = dioSetToken(dio);
      
      if( query != null ) {
        endpoint += endpoint[endpoint.length - 1] == '/' ? '' : '/';

        int index = 0;
        query.forEach((dynamic key, dynamic value) {
          endpoint += (index == 0 ? '?' : '&') + '$key=${parse((parse(value)).body.text).documentElement.text}';
          index++;
        });

      }

      Response response = await dio.get<dynamic>(endpoint);

      return response;
    } on SocketException catch (e) {
      // Do Something
    }
  }

  Future post({BuildContext context, String endpoint, Map data}) async {
    try {

      if( token != null ) dio = dioSetToken(dio);

      Response response = await dio.post<dynamic>(endpoint, data: data);

      return response;
    } on SocketException catch (e) {
      // Do Something
    }
  }

  Future patch({BuildContext context, String endpoint, Map data}) async {
    try {

      if( token != null ) dio = dioSetToken(dio);

      Response response = await dio.patch<dynamic>(endpoint, data: data);

      return response;
    } on SocketException catch (e) {
      // Do Something
    }
  }

  Future put({BuildContext context, String endpoint, Map data}) async {
    try {

      if( token != null ) dio = dioSetToken(dio);

      Response response = await dio.put<dynamic>(endpoint, data: data);

      return response;
    } on DioError {
      // Implement DioError
    }
  }

  Future delete({BuildContext context, String endpoint, Map query}) async {
    try {

      if( token != null ) dio = dioSetToken(dio);

      Response response =
          await dio.delete<dynamic>(endpoint, queryParameters: query);

      return response;
    } on SocketException catch (e) {
      // Do Something
    }
  }

  void setToken( String _token ) {
    token = _token;
  }

  Dio dioSetToken(Dio dio) {
    dio.options.headers['authorization'] = 'Bearer $token';

    return dio;
  }
}
